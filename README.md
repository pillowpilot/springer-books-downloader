Automatize url extraction from a specific PDF and its corresponding download.

> pip install lxml requests pdfminer.six pandas

These books where listed, however they were not available for downloading when I checked.
- [Business Statistics for Competitive Advantage with Excel 2016](https://link.springer.com/book/10.1007%2F978-3-319-32185-1)
- [Literature and Medicine](http://link.springer.com/openurl?genre=book&isbn=978-3-030-19128-3)

Update
------
For download only:
1. Install the downloader dependencies: ``pip install request pandas``
2. Run the downloader: ``python downloader.py``. This will take a while.

Instrucciones detalladas en Español
-----------------------------------

- Instalar el lenguaje Python y sus componentes. Para Windows descargar e instalar: [Python](https://www.python.org/ftp/python/3.8.2/python-3.8.2.exe)

- Ir [aquí](https://gitlab.com/pillowpilot/springer-books-downloader/-/archive/master/springer-books-downloader-master.zip), esto descargará el programa para conseguir los libros automáticamente y no hacerlo manualmente.

- Descomprimir el archivo zip que se descargó y entrar en la carpeta creada. Una vez ahí hacer clic derecho y buscar opción de abrir CMD (consola de comandos). 
Si NO hay esa opción, usar la combinación de teclas Windows+R y en Ejecutar escribir cmd, esto abrirá la consola de comandos. Aquí usar el comando dir para mostrar la carpeta donde está y su contenido. Usando el comando cd acompañado del nombre de la carpeta donde está lo que se descargó (Ejemplo: cd Desktop\springer-books-downloader-master) ir de carpeta en carpeta hasta llegar al destino.

- Una vez en esa carpeta usar el comando mkdir books

- Estos 3 son para instalar las librerías y que nos funcione el programita-
- Usar el comando pip install wheels

- Usar el comando pip install requests

- Usar el comando pip install pandas

- Ahora si a usar nuestro programita: con el comando python downloader.py ya debería empezar a bajar todos los archivos en la carpeta books que creamos anteriormente.

- Esperar a que descargue todo escuchando buena música mientras.

- Ya ta. Sigamos liberando el conocimiento.
  
Obs
---
- Some symbols (`:`, `/`) have been replaced for `-`.
- Filename format is `Title(isbn).pdf`.
- Total download size is approx 8.1GB.
- All 406 books are downloaded. Some books have several editions.
- The average size is 20MB and only 12 books are above 100MB.
- The books with several editions are:
  1. Ceramic Materials.
  2. Fundamentals of Biomechanics.
  3. Transmission Electron Microscopy.
  4. Introduction to Partial Differential Equations.
  5. Introduction to Logic Circuits & Logic Design with Verilog.
  6. Additive Manufacturing Technologies.
  7. Computational Physics.
  8. Fundamentals of Business Process Management.
  9. Strategic International Management.
  10. Robotics, Vision and Control.
  11. Probability Theory.
  12. Quantum Mechanics.
  13. Robotics.
  14. Pharmaceutical Biotechnology.
  15. Introduction to Logic Circuits & Logic Design with VHDL.
  16. Advanced Organic Chemistry.

