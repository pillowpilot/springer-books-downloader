import urllib.request as rq
from pdfminer.high_level import extract_text

def main(pdf_filepath):
    txt = extract_text(pdf_filepath)
    # print(txt)
    urls = []
    for line in txt.splitlines():
        if 'springer.com' not in line:
            continue
        try:
            request = rq.urlopen(line)
            print(f'Detected url: {line}.')
            print(request)
            urls.append(line)
        except ValueError as ex:
            print(f'Rejected: {line}')
        except RuntimeError as ex:
            print(ex)

    print(f'Detected {len(urls)} urls.')
    with open('urls.txt', 'w') as output_file:
        output_file.writelines(map(lambda x: x + '\n', urls))


if __name__ == '__main__':
    pdf_filepath = './springer_books.pdf'
    main(pdf_filepath)
