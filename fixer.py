import os
import pandas as pd
import requests


def main(csv_filepath, store_directory):
    duplicates = [('Ceramic Materials', 2), ('Fundamentals of Biomechanics', 2), ('Transmission Electron Microscopy', 2), ('Introduction to Partial Differential Equations', 2), ('Introduction to Logic Circuits & Logic Design with Verilog', 2), ('Additive Manufacturing Technologies', 2), ('Computational Physics', 2), ('Fundamentals of Business Process Management', 2), ('Strategic International Management', 2), ('Robotics, Vision and Control', 2), ('Probability Theory', 2), ('Quantum Mechanics', 2), ('Robotics', 2), ('Pharmaceutical Biotechnology', 2), ('Introduction to Logic Circuits & Logic Design with VHDL', 2), ('Advanced Organic Chemistry', 2)]
    duplicates = [x[0] for x in duplicates]
    print(duplicates)

    data = pd.read_csv(csv_filepath)
    print(data.head())
    for index, row in data.iterrows():
        url: str = row['download_links']
        isbn: str = row['isbn']
        title: str = row['titles']
        title = title.replace('/', '-').replace(':', '-')
        filepath = os.path.join(store_directory, f'{title}.pdf')
        if os.path.exists(filepath) and title not in duplicates:
            new_filepath = os.path.join(store_directory, f'{title}({isbn}).pdf')
            print(f'Renaming {filepath} to {new_filepath}')
            # os.rename(filepath, new_filepath)
        else:
            pass


if __name__ == '__main__':
    csv_filepath = './download_links.csv'
    store_directory = './books'  # This directory must exists!
    main(csv_filepath, store_directory)