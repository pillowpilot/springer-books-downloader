import os
import pandas as pd
import requests


def main(csv_filepath, store_directory):
    data = pd.read_csv(csv_filepath)
    print(data.head())
    for index, row in data.iterrows():
        url: str = row['download_links']
        isbn: str = row['isbn']
        title: str = row['titles']
        title = title.replace('/', '-').replace(':', '-')
        filepath = os.path.join(store_directory, f'{title}({isbn}).pdf')
        if os.path.exists(filepath):
            print(f'File {filepath} already exists! Skipping.')
        else:
            print(f'Trying to download {title} from {url} to {filepath}.')
            r = requests.get(url, allow_redirects=True)
            open(filepath, 'wb').write(r.content)
            print('Done.')


if __name__ == '__main__':
    csv_filepath = './download_links.csv'
    store_directory = './libros'
    if not os.path.exists(store_directory):
        os.mkdir(store_directory)
    if not os.path.exists(csv_filepath):
        print(f'CSV File ({csv_filepath}) not found!')
    else:
        main(csv_filepath, store_directory)
