from lxml import html
import pandas as pd
import requests


def main(urls_filepath, csv_filepath):
    urls = None
    with open(urls_filepath, 'r') as urls_file:
        urls = urls_file.readlines()

    retry_urls = []
    data = {'titles': [], 'download_links': [], 'isbn': []}
    for url in urls:
        try:
            page = requests.get(url)
            tree = html.fromstring(page.content)
            title = tree.xpath('//div[@class="page-title"]/h1/text()')
            title = title[0].lstrip(' ').rstrip(' ')
            print(title)

            link = tree.xpath('//a[@title="Download this book in PDF format"]')
            link = 'http://link.springer.com' + link[0].get('href')
            print(link)

            isbn = url[url.find('isbn') + 5:-1]
            print(isbn)

            data.get('titles').append(title)
            data.get('download_links').append(link)
            data.get('isbn').append(isbn)
        except IndexError as ex:
            print(ex)
            print(f'Retry: {url}')
            retry_urls.append(url)

    pd.DataFrame(data=data).to_csv(csv_filepath)


if __name__ == '__main__':
    urls_filepath = './urls.txt'
    csv_filepath = './download_links.csv'
    main(urls_filepath, csv_filepath)
